<?php

/**
 * Created by PhpStorm.
 * User: Saeed Shahini
 * Date: 8/15/2016
 * Time: 11:21 PM
 */

class DatabaseManager
{
    const DATABASE_NAME = "sepehr_db";

    function createDatabase()
    {
        $connection = mysqli_connect("localhost", "root", "");
        $sqlCommand = "CREATE DATABASE " . DatabaseManager::DATABASE_NAME;
        if (mysqli_query($connection, $sqlCommand)) {
            echo "Database created successfully";
        }else{
            echo "error occured in creating database";
        }
    }


    function createPostsTable()
    {
        $connection = mysqli_connect("localhost", "root", "", DatabaseManager::DATABASE_NAME);
        $sqlCommand = "CREATE TABLE sepehr (code INTEGER PRIMARY KEY ,
                              title TEXT)";
        if (mysqli_query($connection, $sqlCommand)) {
            echo "Post table created successfully";
        }else{
            echo "error occured in creating table";
        }
    }

    function addPost($code,$title){
        $connection=mysqli_connect("localhost","root","",DatabaseManager::DATABASE_NAME);
        $sqlCommand="INSERT INTO sepehr(code,title) VALUES('$code','$title')";
        if (mysqli_query($connection,$sqlCommand)){
            echo "Post added to table successfully";
        }else{
            echo "Error in adding post to table";
        }
    }


    function getPosts($offset,$count){
        $connection=mysqli_connect("localhost","root","",DatabaseManager::DATABASE_NAME);

        $sqlQuery="SELECT * FROM sepehr";

        $result=$connection->query($sqlQuery);

        $postsArray=array();
        if ($result->num_rows>0){
            for ($i=$offset;$i<$count+$offset;$i++){
                $postsArray[$i]=$result->fetch_assoc();
            }
        }

        echo json_encode($postsArray);
    }

}